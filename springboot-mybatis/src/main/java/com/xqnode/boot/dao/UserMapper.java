package com.xqnode.boot.dao;

import com.xqnode.boot.model.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}