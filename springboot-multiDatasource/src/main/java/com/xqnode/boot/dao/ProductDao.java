package com.xqnode.boot.dao;

import cn.hutool.db.DbUtil;
import com.xqnode.boot.entity.Product;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ProductDao {

    @Resource(name = "mytestDataSource")
    DataSource mytestDataSource;

    public List<Product> findProductByPriceAfter(Double price) throws SQLException {
        String sql = "select * from t_product where price > ?";
        return DbUtil.use(mytestDataSource).query(sql, Product.class, price);
    }
}
