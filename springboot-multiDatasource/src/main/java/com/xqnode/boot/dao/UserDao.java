package com.xqnode.boot.dao;

import cn.hutool.db.DbUtil;
import com.xqnode.boot.entity.User;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;

@Repository
public class UserDao {

    @Resource(name = "testDataSource")
    private DataSource testDataSource;

    public User findUserByLoginNameAndPassword(String loginName, String password) throws SQLException {
        String sql = "select * from t_user where login_name = ? and password = ?";
        return DbUtil.use(testDataSource).queryOne(sql, loginName, password).toBean(User.class);
    }

}
