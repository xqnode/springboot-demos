package com.xqnode.boot.dao;

import com.xqnode.boot.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJpaDao extends JpaRepository<User, Integer> {
}
