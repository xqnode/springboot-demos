package com.xqnode.boot.controller;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.xqnode.boot.common.JsonResult;
import com.xqnode.boot.dao.UserDao;
import com.xqnode.boot.dao.UserJpaDao;
import com.xqnode.boot.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * created by xiaqing on 2019/3/6 20:11
 */
@Api(tags = "用户相关接口")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserJpaDao userJpaDao;

    @ApiOperation("用户登录")
    @PostMapping("/login")
    public JsonResult login(@ApiParam(name = "用户请求实体")
                            @RequestBody User user) throws Exception {
        String loginName = user.getLoginName();
        String passwordMd5 = SecureUtil.md5(user.getPassword());
        User dbUser = userDao.findUserByLoginNameAndPassword(loginName, passwordMd5);
        if (dbUser == null) {
            throw new RuntimeException("用户名或密码错误");
        }
        return JsonResult.dataResult("登录成功");
    }

    @ApiOperation("用户注册")
    @PostMapping("/registry")
    public JsonResult registry(@ApiParam(name = "用户请求实体")
                               @RequestBody User userDTO) throws Exception {
        if (StrUtil.isBlank(userDTO.getLoginName()) || StrUtil.isBlank(userDTO.getPassword())) {
            throw new RuntimeException("用户名和密码不能为空");
        }
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        user.setPassword(SecureUtil.md5(userDTO.getPassword()));
        user.setCreateTime(new Date());
        userJpaDao.save(user);
        return JsonResult.dataResult(user);
    }
}
