package com.xqnode.boot.controller;

import com.xqnode.boot.common.JsonResult;
import com.xqnode.boot.dao.ProductDao;
import com.xqnode.boot.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductDao productDao;

    @GetMapping("/priceAfter/{price}")
    public JsonResult query(@PathVariable Double price) throws SQLException {
        List<Product> productList = productDao.findProductByPriceAfter(price);
        return JsonResult.dataResult(productList);
    }
}
