package com.xqnode.boot.config;

import com.xqnode.boot.service.CommonService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;

@Configuration
public class WebserviceConfig {

    @Resource
    private Bus bus;

    @Resource
    private CommonService commonService;

    /**
     * JAX-WS  对外发布 commonService服务
     * 使用网址：http://localhost:8080/services/commonService?wsdl
     * 获取所有服务
     **/
    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, commonService);
        endpoint.publish("/commonService");
        return endpoint;
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    // @Bean // 修改默认的servlet名称为test 默认为services
    // public ServletRegistrationBean dispatcherServlet() {
    // return new ServletRegistrationBean(new CXFServlet(), "/test/*");
    // }
    //
    // @Bean(name = Bus.DEFAULT_BUS_ID)
    // public SpringBus springBus() {
    // return new SpringBus();
    // }
    //
    // @Bean
    // public UserService userService() {
    // return new UserServiceImpl();
    // }
    //
    // @Bean
    // public Endpoint endpoint() {
    // EndpointImpl endpoint = new EndpointImpl(springBus(), userService());
    // endpoint.publish("/user");
    // return endpoint;
    // }
}