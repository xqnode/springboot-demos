package com.xqnode.boot.util;

import cn.hutool.core.codec.Base64;
import com.xqnode.boot.service.CommonService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

/**
 * @author 夏青
 */
public class CxfUtil {
    public static void main(String[] args) throws Exception {
        cl2();
    }

    /**
     * 方式1.代理类工厂的方式,需要拿到对方的接口
     */
    public static void cl1(String param) {
        try {
            // 接口地址
            String address = "http://localhost:8080/services/CommonService?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(CommonService.class);
            // 创建一个代理接口实现
            CommonService cs = (CommonService) jaxWsProxyFactoryBean.create();
            // 数据准备
//            String userName = "Leftso";
            // 调用代理接口的方法调用并返回结果
            String result = cs.sayHello(param);
            String decodeStr = Base64.decodeStr(result);
            System.out.println("返回结果:\n" + decodeStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 动态调用方式
     *
     * @throws Exception
     */
    public static void cl2() throws Exception {
        // 创建动态客户端
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf
                .createClient("http://localhost:8080/services/commonService?wsdl");
        // getUser 为接口中定义的方法名称 张三为传递的参数 返回一个Object数组
        Object[] objects = client.invoke("sayHello", "张三");
        for (Object object : objects) {
            String ss = Base64.decodeStr((String) object);
            System.err.println(ss);
        }

        // 输出调用结果
        System.out.println(objects[0].toString());
    }
}