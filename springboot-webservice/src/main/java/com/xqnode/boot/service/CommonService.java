package com.xqnode.boot.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * created by xiaqing on 2019/3/6 22:49
 */
@WebService(name = "CommonService", // 与接口中指定的name一致
        targetNamespace = "http://boot.xqnode.com/")
public interface CommonService {

    @WebMethod
    String sayHello(@WebParam(name = "param") String param);
}
