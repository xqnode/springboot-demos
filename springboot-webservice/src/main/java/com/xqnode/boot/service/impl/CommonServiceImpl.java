package com.xqnode.boot.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.XmlUtil;
import com.xqnode.boot.service.CommonService;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.jws.WebService;

/**
 * 接口实现
 *
 * @author leftso
 */
@WebService(serviceName = "CommonService", // 与接口中指定的name一致
        targetNamespace = "http://boot.xqnode.com/", // 与接口中的命名空间一致,一般是接口的包名倒
        endpointInterface = "com.xqnode.boot.service.CommonService"// 接口地址
)
@Component
public class CommonServiceImpl implements CommonService {

    @Override
    public String sayHello(String param) {
//        String decodeStr = Base64.decodeStr(param);
//        Document document = XmlUtil.parseXml(decodeStr);
//        Element rootElement = XmlUtil.getRootElement(document);
        // 获取ahdm
//        String ahdm = Base64.decodeStr(XmlUtil.elementText(rootElement, "AHDM"));

        //创建xml
        Document doc = XmlUtil.createXml();
        doc.setXmlStandalone(true);

        Element response = doc.createElement("Response");
        doc.appendChild(response);
        Element result = doc.createElement("Result");
        response.appendChild(result);
        Element codeEle = doc.createElement("Code");
        codeEle.setTextContent(Base64.encode("0"));
        result.appendChild(codeEle);
        Element msgEle = doc.createElement("Msg");
        msgEle.setTextContent(Base64.encode("操作成功"));
        result.appendChild(msgEle);

        Element data = doc.createElement("DATA");
        data.setAttribute("Count", Base64.encode("1"));
        result.appendChild(data);

        Element eaj = doc.createElement("EAJ");
        data.appendChild(eaj);

        Element ahdmEle = doc.createElement("AHDM");
        ahdmEle.setTextContent(Base64.encode("2"));
        eaj.appendChild(ahdmEle);

        Element ah = doc.createElement("AH");
        ah.setTextContent(Base64.encode("（2018）沪0107民初12723号"));
        eaj.appendChild(ah);

        Element fydm = doc.createElement("FYDM");
        fydm.setTextContent(Base64.encode("226000"));
        eaj.appendChild(fydm);

        String docStr = XmlUtil.toStr(doc);
        System.out.println(docStr);
        return Base64.encode(docStr);
    }

}