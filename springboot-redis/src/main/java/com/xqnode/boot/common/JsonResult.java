package com.xqnode.boot.common;

public class JsonResult {

    private String success;
    private String message;
    private Object data;

    private static final String SUCCESS = "0";
    private static final String SUCCESS_MESSAGE = "success";
    private static final String ERROR = "1";

    public JsonResult(String success, String message) {
        this.success = success;
        this.message = message;
    }

    public static JsonResult defaultResult() {
        return new JsonResult(SUCCESS, SUCCESS_MESSAGE);
    }

    public static JsonResult dataResult(Object data) {
        JsonResult jsonResult = defaultResult();
        jsonResult.setData(data);
        return jsonResult;
    }

    public static JsonResult errorResult(String errorMessage) {
        return new JsonResult(ERROR, errorMessage);
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
