package com.xqnode.boot.config;

import com.xqnode.boot.interceptor.SpringWebSocketHandlerInterceptor;
import com.xqnode.boot.server.MyWebSocketHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import javax.annotation.Resource;

/**
 * @author 夏青
 */
@Configuration
public class WebSocketHandlerConfig implements WebSocketConfigurer {

    @Resource
    private MyWebSocketHandler myWebSocketHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(myWebSocketHandler, "/socket")
                .addInterceptors(new SpringWebSocketHandlerInterceptor())
                .setAllowedOrigins("*")
//                .withSockJS()
        ;
    }
}
