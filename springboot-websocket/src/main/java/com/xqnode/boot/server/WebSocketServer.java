package com.xqnode.boot.server;

import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

/**
 * @author 夏青
 * 第二种方式： 发布websocket服务启动websocket
 */
@Component
@ServerEndpoint(value = "/websocket")
public class WebSocketServer {

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen() {
        System.out.println("websocket连接成功！");
    }

    @OnMessage
    public void onMessage(String msg) {
        System.out.println("websocket收到消息：" + msg);
    }

    @OnClose
    public void onClose() {
        System.out.println("websocket服务关闭");
    }

}
