package com.xqnode.boot.dao;

import com.xqnode.boot.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * created by xiaqing on 2019/3/6 21:16
 */
public interface UserDao extends JpaRepository<User, Integer> {

    User findUserByLoginNameAndPassword(String loginName, String password);
}
