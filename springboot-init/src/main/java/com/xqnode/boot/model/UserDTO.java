package com.xqnode.boot.model;

import javax.validation.constraints.NotBlank;

/**
 * created by xiaqing on 2019/3/6 20:08
 */
public class UserDTO {

    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 登录名
     */
    private String loginName;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 联系地址
     */
    private String addr;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
}
